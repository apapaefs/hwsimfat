# -*- Makefile -*- (for emacs)
#
# This Makefile is intended for compiling Herwig++ plugins
#
# This Makefile received very little testing, 
# any bug reports are very welcome!
#
# Location of include files

THEPEGINCLUDE=-I/Users/apapaefs/Documents/Projects/Herwig-stable/include
HERWIGINCLUDE=-I/Users/apapaefs/Documents/Projects/Herwig-stable/include/
GSLINCLUDE=-I/Users/apapaefs/Documents/Projects/Herwig-stable/include 
HERWIGINSTALL=/Users/apapaefs/Documents/Projects/Herwig-stable

ROOTINCLUDE = -I$(shell root-config --incdir)
ROOTCFLAGS  := $(shell root-config --ldflags) 
ROOTGLIBS   := $(shell root-config --glibs) -lGenVector

FASTJET = $(shell fastjet-config --prefix)
FASTJETINCLUDE=-I/Users/apapaefs/Documents/Projects/Herwig-stable/include -I$(FASTJET)/include/ -I$(FASTJET)/include/fastjet
FASTJETLIB = $(shell $(FASTJET)/bin/fastjet-config --libs) -lNsubjettiness

LDFLAGS= $(FASTJETLIB) $(ROOTCFLAGS) $(ROOTGLIBS)
SHARED_FLAG=-bundle -Wl,-undefined,dynamic_lookup
INCLUDE = $(THEPEGINCLUDE) $(HERWIGINCLUDE) $(GSLINCLUDE) $(ROOTINCLUDE) $(FASTJETINCLUDE)

# C++ flags
CXX=g++ -std=c++11 -std=c++11 
CXXFLAGS=-O2 -DBOOST_UBLAS_NDEBUG

ALLCCFILES=$(shell echo *.cc)

default : HwSimFat.so

%.o : %.cc %.h
	$(CXX) -fPIC $(CPPFLAGS) $(INCLUDE) $(CXXFLAGS) $(SHARED_FLAG) $(LDFLAGS) -c $< -o $@  

HwSimFat.so : HwSim.o HwSimAnalysis.h
	$(CXX) -o HwSimFat.so HwSim.o -fPIC $(CPPFLAGS) $(INCLUDE) $(CXXFLAGS) $(SHARED_FLAG) $(LDFLAGS) \
	-Wl,-undefined -Wl,dynamic_lookup \


clean:
	rm -f $(ALLCCFILES:.cc=.o) \
		HwSimFat.so
 
install:
	cp *.so $(HERWIGINSTALL)/lib/Herwig	
