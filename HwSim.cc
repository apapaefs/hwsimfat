// -*- C++ -*-
//
// HwSim.cc is a part of Herwig - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2016 The Herwig Collaboration
//
// Herwig++ is licenced under version 2 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
//
// This is the implementation of the non-inlined, non-templated member
// functions of the HwSim class.
//

#include <cmath>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#include "HwSim.h"
#include "Herwig/Utilities/Histogram.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Vectors/ThreeVector.h"
#include "ThePEG/EventRecord/Event.h"
#include "ThePEG/PDT/EnumParticles.h"
#include "ThePEG/Interface/Switch.h"
#include "ThePEG/Interface/Reference.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Interface/Parameter.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

#include "TTree.h"
#include "TFile.h"
#include "TStyle.h"
#include "TH1F.h"
#include "TRandom3.h"

//Fastjet headers
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include <fastjet/tools/JHTopTagger.hh>
#include <fastjet/Selector.hh>
//Boost headers
#include <boost/algorithm/string.hpp>
#include <boost/tuple/tuple.hpp>
//costum headers
#include "complex_d.h"


using namespace std;
using namespace pdf;
using namespace fastjet;
using namespace Herwig;

HwSimFat::HwSimFat()
  :jetAlgorithm_(-1), cut_eta_(5.0), cut_pt_part_(0.1), cut_pt_jet_(20.0), cut_eta_jet_(5.0), cut_eta_electron_(3.0),cut_pt_electron_(10.0), cut_eta_muon_(3.0), cut_pt_muon_(10.0), cut_pt_photon_(20.0),cut_eta_photon_(5.0), R_(0.4), saveObjects_(0), saveReconstructed_(0), ctagging_(0), fl_DeltaR_(0.4), fl_PartonPTMin_(1.), fl_PartonEtaMax_(6), onfly_(0), saveoptionalweights_(0), electronisolation_(0), muonisolation_(0), muonIsoFrac_(0.1), muonIsoR_(0.2), electronIsoFrac_(0.1), electronIsoR_(0.2), btaggingmethod_(0), rootoutlocation_(""), FatR_(0.8), fatjets_(0), zcut_(0.1), rcut_factor_(0.5), pruneFat_(0), cut_pt_fatjet_(200.0), cut_eta_fatjet_(5.0)
{}

IBPtr HwSimFat::clone() const {
  return new_ptr(*this);
}

IBPtr HwSimFat::fullclone() const {
  return new_ptr(*this);
}

void HwSimFat::persistentOutput(PersistentOStream & os) const {
  os  << jetAlgorithm_ << cut_eta_ << cut_pt_part_ << cut_pt_jet_ << cut_eta_jet_ << cut_eta_electron_ << cut_pt_electron_ << cut_eta_muon_ << cut_pt_muon_ << cut_pt_photon_ << cut_eta_photon_ << R_ << saveReconstructed_ << saveObjects_ << ctagging_ << onfly_ << saveoptionalweights_ << electronisolation_ << muonisolation_ << muonIsoFrac_ << muonIsoR_ << electronIsoFrac_ << electronIsoR_ << btaggingmethod_ << rootoutlocation_ << FatR_ << fatjets_ << zcut_ << rcut_factor_ <<  pruneFat_ << cut_pt_fatjet_ << cut_eta_fatjet_;

}

void HwSimFat::persistentInput(PersistentIStream & is, int) {
  is >> jetAlgorithm_ >> cut_eta_ >> cut_pt_part_ >> cut_pt_jet_ >> cut_eta_jet_ >> cut_eta_electron_ >> cut_pt_electron_ >> cut_eta_muon_ >> cut_pt_muon_ >> cut_pt_photon_ >> cut_eta_photon_ >> R_ >> saveReconstructed_ >> saveObjects_ >> ctagging_ >> onfly_ >> saveoptionalweights_ >> electronisolation_ >> muonisolation_ >> muonIsoFrac_ >> muonIsoR_ >> electronIsoFrac_ >> electronIsoR_ >> btaggingmethod_ >> rootoutlocation_ >> FatR_ >> fatjets_ >> zcut_ >> rcut_factor_ >>  pruneFat_ >> cut_pt_fatjet_ >> cut_eta_fatjet_;
}

bool pTsortFunction(PPtr i,PPtr j) {
  return (i->momentum().perp2()>j->momentum().perp2());
}
bool ETsortFunction(pair<Energy, Lorentz5Momentum> i,pair<Energy, Lorentz5Momentum> j) {
  return (i.first>j.first);
}

/***********************************************************************
 * MAIN ANALYSIS FUNCTION
************************************************************************/
void HwSimFat::analyze(tEventPtr event, long, int, int) {

  
  /*
   * COUNT THE TOTAL NUMBER OF EVENTS
   */
  numevents_++;

  /* 
   * GET THE EVENT WEIGHT
   */ 
  evweight = event->weight();

  /*
   * PROCESS OPTIONAL WEIGHTS IF ASKED TO SAVE
   */

  if(saveoptionalweights_) {
    theOptWeightsNames->clear();
    theOptWeights->clear();
    for (map<string,double>::const_iterator it= event->optionalWeights().begin(); it!=event->optionalWeights().end(); ++it){
      string optweight_string = it->first; // contains the weight information
      double optweight_value = it->second; // contains the weight
      theOptWeightsNames->push_back(optweight_string);
      theOptWeights->push_back(optweight_value);
    }
  }
  
  /*
   * GET THE FINAL-STATE PARTICLES
   */
  event->getFinalState( particlesToCluster_ );
  numparticles = particlesToCluster_.size();  //get the total number of particles

  /*
   * GET THE VECTOR OF INTERMEDIATE PARTICLES
   * IN HARD PROCESS AND LOOP OVER THEM
   * (FOR FUTURE USE)
   */ 
  ParticleVector intermediates(event->primarySubProcess()->outgoing());
  //search primary collision
  StepVector::const_iterator sit =event->primaryCollision()->steps().begin();
  StepVector::const_iterator send=event->primaryCollision()->steps().end(); 
  ParticleSet partW=(**sit).all();
  ParticleSet::const_iterator iterrW=partW.begin();
  ParticleSet::const_iterator enddW =partW.end();
  int ncharm = 0;
  Lorentz5Momentum pcc(0.*GeV,0.*GeV,0.*GeV,0.*GeV,0.*GeV);

  if(ctagging_) {
    for(unsigned int ww = 0; ww < intermediates.size(); ww++) { 
      if(fabs(intermediates[ww]->id()) == 4) {
        ncharm++;
	//   cout << "charm quark found!" << ncharm << endl;
        pcc = intermediates[ww]->momentum();
        //cout << "momentum: x,y,z,e = " << pcc.x()/GeV << " " << pcc.y()/GeV << " " << pcc.z()/GeV << " " << pcc.e()/GeV << endl;
        ThePEG::tPPtr test_current;
        test_current = intermediates[ww];
        while( (test_current->children().size() == 2 && (fabs(test_current->children()[0]->id()) == 4 || fabs(test_current->children()[1]->id()) == 4)) || (test_current->children().size() == 1 && fabs(test_current->children()[0]->id()) == 4) ) {
          if(fabs(test_current->children()[0]->id()) == 4) {
            test_current = test_current->children()[0];
          } else {
            if(test_current->children().size() > 1) { 
              test_current = test_current->children()[1];
            }
          }
        }
        charms.push_back(test_current);
        pcc = test_current->momentum();
	//  cout << "final momentum: x,y,z,e = " << pcc.x()/GeV << " " << pcc.y()/GeV << " " << pcc.z()/GeV << " " << pcc.e()/GeV << endl;
      }
    }
  }



  /* 
   * LOOP OVER FINAL-STATE PARTICLES AND PUT THEM IN 
   * THE OBJECTS ARRAY
   */
  int Id = 0, absId = 0;
  Lorentz5Momentum pz;
  int Charge = 0;
  for(unsigned int ii = 0; ii < particlesToCluster_.size(); ii++) {

    absId=fabs(particlesToCluster_[ii]->id()); //get abs(id) of particle
    Id = particlesToCluster_[ii]->id(); //id of particle
    Charge = particlesToCluster_[ii]->dataPtr()->iCharge(); //charge of particle
    pz = particlesToCluster_[ii]->momentum(); //put the momentum in a Lorentz5Momentum
    objects[0][ii] = pz.e()/GeV; //energy
    objects[1][ii] = pz.x()/GeV; //px
    objects[2][ii] = pz.y()/GeV; //py 
    objects[3][ii] = pz.z()/GeV; //pz
    objects[4][ii] = Id; //Id
    objects[5][ii] = Charge/3.0; //Charge +-1, 0
    objects[6][ii] = 0;//extra identifiers (for future use)
    objects[7][ii] = 0;
  }
    
  
  /* FIND JETS, ELECTRONS, MUONS 
   * AND PASS TO ANALYSIS
   */

  /* only proceed if this flag is true */
  if(!saveReconstructed_) {
    nevpass++;
    nevpass_weighted+=evweight;
    Data->Fill();
    //clear particle vectors for next use
    particlesToCluster_.clear();
    return;
  }

  /* global cut on all particles and min cut on jets */
  double cut_eta(cut_eta_); //global pseudorapidity cut
  double cut_pt_part(cut_pt_part_); //global pt cut for particles [GeV]

  /* jet cuts */
  double cut_pt_jet(cut_pt_jet_); //pt cut for jets
  double cut_eta_jet(cut_eta_jet_); //pseudo-rapidity cut for jets

  /* lepton cuts */
  double cut_eta_electron(cut_eta_electron_); // pseudo-rapidity cut for electrons
  double cut_pt_electron(cut_pt_electron_); // pt cut for electrons

  double cut_eta_muon(cut_eta_muon_); // pseudo-rapidity cut for muons
  double cut_pt_muon(cut_pt_muon_); // pt cut for muons

  /* photon cuts */
  double cut_pt_photon(cut_pt_photon_); //pt cut for jets
  double cut_eta_photon(cut_eta_photon_); //pseudo-rapidity cut for jets

  /* jet-flavour association parameters */
  double fl_DeltaR(fl_DeltaR_);
  double fl_PartonPTMin(fl_PartonPTMin_);
  double fl_PartonEtaMax(fl_PartonEtaMax_);

  /*jet algorithm */
  double R(R_);
  
  /* particles to exclude from jet finder:
   * neutrinos
   */
  int excluded[6] = { 12, -12, 14, -14, 16, -16 } ;
  
  vector<int> exclude_ids;
  for(int ex = 0; ex < 6; ex++) exclude_ids.push_back(excluded[ex]);
						
  fastjet::PseudoJet pcurr; //the current particle under consideration
  vector<fastjet::PseudoJet> input_particles; //the input particles to the jet finder

  vector<fastjet::PseudoJet> lepton, lepton_pt_unsorted, lepton_pt; //all leptons and the leptons passing cuts
  vector<int> lepton_id; //the lepton ids
    
  vector<fastjet::PseudoJet> photon; //all photons passing cuts
  
  vector<fastjet::PseudoJet> higgses; //all undecayed higgs bosons
    
  fastjet::PseudoJet pmiss; //vectors of bs and Ws



  /* loop over particles in event
   * decide which are "photons" and "leptons", 
   * according to isolation criteria. 
   * these are then excluded from the jet finder. 
   */
  bool exclude_particle(false);
  for(int pp = 0; pp < numparticles; pp++) {
    
    //put the current particle into a pseudojet vector
    pcurr = fastjet::PseudoJet(objects[1][pp], objects[2][pp], objects[3][pp], objects[0][pp]);
    pcurr.set_user_index(int(objects[4][pp]));
   
    // if the particle is included in the array above, flag as excluded from particles to run over jet-finding
    for(int idl = 0; idl < exclude_ids.size(); idl++) {
      if(fabs(objects[4][pp]) == exclude_ids[idl]) { exclude_particle = true; }
    }
            
    /* FIND HARD LEPTONS 
     * if they do not satisfy the cuts, put them into the jet finder. 
     */
    fastjet::PseudoJet pcurr_charged; //the current particle under consideration
    //loop over particles
    if(fabs(objects[4][pp]) == 11 && pcurr.perp() > cut_pt_electron && fabs(pcurr.eta()) < cut_eta_electron) {
      if(!electronisolation_) {
        lepton.push_back(pcurr); 
	lepton_id.push_back(int(objects[4][pp]));
        exclude_particle = true;
      } else if(electronisolation_) {
	bool iso_electron = 1;
	double sum_pt_tracks = 0;
	for(int yy = 0; yy < numparticles; yy++) {
	  if(objects[5][yy]!=0 && yy!=pp) {//if charged particle
	    pcurr_charged = fastjet::PseudoJet(objects[1][yy], objects[2][yy], objects[3][yy], objects[0][yy]);
	    if(deltaR(pcurr, pcurr_charged) < electronIsoR_)
	      sum_pt_tracks += pcurr_charged.perp();
	  }
	}
	if(sum_pt_tracks/pcurr.perp() < electronIsoFrac_) {
	  //cout << "electron isolation = " << sum_pt_tracks/pcurr.perp()  << endl;	
	  iso_electron = 1;
	  lepton.push_back(pcurr); 
	  lepton_id.push_back(int(objects[4][pp]));
	  exclude_particle = true;
	}	  
      }
    }
	
    if(fabs(objects[4][pp]) == 13 && pcurr.perp() > cut_pt_muon && fabs(pcurr.eta()) < cut_eta_muon) {
      if(!muonisolation_) {
        lepton.push_back(pcurr); 
	lepton_id.push_back(int(objects[4][pp]));
        exclude_particle = true;
      } else if(muonisolation_) {
	bool iso_muon = 1;
	double sum_pt_tracks = 0;
	for(int yy = 0; yy < numparticles; yy++) {
	  if(objects[5][yy]!=0&&yy!=pp) {//if charged particle
	    pcurr_charged = fastjet::PseudoJet(objects[1][yy], objects[2][yy], objects[3][yy], objects[0][yy]);
	    if(deltaR(pcurr, pcurr_charged) < muonIsoR_)
	      sum_pt_tracks += pcurr_charged.perp();
	  }
	}
	if(sum_pt_tracks/pcurr.perp() < muonIsoFrac_) {
	  //cout << "muon isolation = " << sum_pt_tracks/pcurr.perp()  << endl;	
	  iso_muon = 1;
	  lepton.push_back(pcurr); 
	  lepton_id.push_back(int(objects[4][pp]));
	  exclude_particle = true;
	}	  
      }
    }
    
    if( (fabs(objects[4][pp]) == 22 && pcurr.perp() > cut_pt_photon && fabs(pcurr.eta()) < cut_eta_photon) ) { 
        photon.push_back(pcurr); 
        exclude_particle = true; 
    }

      if( (fabs(objects[4][pp]) == 25) ) { 
        higgses.push_back(pcurr); 
        exclude_particle = true; 
    }


    // calculate missing energy 4-vector (using all visible objects)  
    if(fabs(pcurr.eta()) < cut_eta && pcurr.perp() > cut_pt_part && fabs(objects[4][pp]) != 12 && fabs(objects[4][pp]) != 14 && fabs(objects[4][pp]) != 16) {
      pmiss -= pcurr;
    }//end of calculate pmiss;
    
    //push the visible particles into the jet finding input and calculate the missing energy 4-vector
    if(fabs(pcurr.eta()) < cut_eta && pcurr.perp() > cut_pt_part && !exclude_particle) {
      input_particles.push_back(pcurr);
    }
    
    //reset the exclude_particle flag
    exclude_particle = false;	    
  } /* LOOP OVER PARTICLES OF EVENT
     * ENDS
     * HERE 
     */

  /* START JET-FINDING
   * USING FASTJET
   */
  fastjet::RecombinationScheme recombinationScheme = fastjet::E_scheme;
  fastjet::Strategy            strategy            = fastjet::Best;
  fastjet::JetDefinition theJetDefinition;

 
  //first figure out which algorithm is chosen
  switch (jetAlgorithm_) {
  case  -1: theJetDefinition=fastjet::JetDefinition(fastjet::antikt_algorithm,
						    R,
						    recombinationScheme,
						    strategy); break;
  case   0: theJetDefinition=fastjet::JetDefinition(fastjet::cambridge_algorithm,
						    R,
						    recombinationScheme,
						    strategy); break;
  case   1: theJetDefinition=fastjet::JetDefinition(fastjet::kt_algorithm,
						    R,
						    recombinationScheme,
						    strategy); break;
  }
  fastjet::ClusterSequence clust_seq(input_particles, theJetDefinition);

  vector<fastjet::PseudoJet> jet = sorted_by_pt(clust_seq.inclusive_jets(cut_pt_jet));

  
  /* translate PseudoJets to 
       double arrays */
  // (E,x,y,z)

  int nj = 0, nb = 0;
  for(int z = 0; z < jet.size(); z++) {
      if(btag_hadrons(jet[z])) {
        thebJets[0][nb] = jet[z].e();
        thebJets[1][nb] = jet[z].px();
        thebJets[2][nb] = jet[z].py();
        thebJets[3][nb] = jet[z].pz();
        nb++;
      } else {
        cTag[nj] = 0;
        theJets[0][nj] = jet[z].e();
        theJets[1][nj] = jet[z].px();
        theJets[2][nj] = jet[z].py();
        theJets[3][nj] = jet[z].pz();
        if(ctagging_) {
          double DRJC(-1.);
          for(int cc = 0; cc < charms.size(); cc++) {
            pcc = charms[cc]->momentum();
            fastjet::PseudoJet cquark = fastjet::PseudoJet(pcc.x()/GeV, pcc.y()/GeV, pcc.z()/GeV, pcc.e()/GeV);
            DRJC = deltaR(cquark, jet[z]);
	    //cout << "DJRC = " << DRJC << endl;
            if(fabs(cquark.eta()) < fl_PartonEtaMax_ && cquark.perp() > fl_PartonPTMin_ && DRJC < fl_DeltaR_) {
              cTag[nj] = 1;
              // cout << "jet number " << nj << " has been c-tagged! DRJC = " << DRJC << endl;
            }
          }
        }
        nj++;
      }
      //cout << "jet " << z << "\t" << jet[z].e() << "\t" << jet[z].px() << "\t" << jet[z].py() << "\t" << jet[z].pz() << endl;
  }
  charms.clear();


  //define counters
  int nm = 0, nam = 0, ne = 0, nae = 0, nh = 0;

  
  //photons done at analysis level. 
  for(int z = 0; z < photon.size(); z++) {
    thePhotons[0][z] = photon[z].e();
    thePhotons[1][z] = photon[z].px();
    thePhotons[2][z] = photon[z].py();
    thePhotons[3][z] = photon[z].pz();
    //cout << "photon " << z << "\t" << photon[z].e() << "\t" << photon[z].px() << "\t" << photon[z].py() << "\t" << photon[z].pz() << endl;
   }

  //Undecayed Higgses
  for(int z = 0; z < higgses.size(); z++) {
    theHiggses[0][z] = higgses[z].e();
    theHiggses[1][z] = higgses[z].px();
    theHiggses[2][z] = higgses[z].py();
    theHiggses[3][z] = higgses[z].pz();
    nh++;
   }

  
  for(int z = 0; z < lepton.size(); z++) {  
    if(lepton_id[z] == 13) {
      theMuons[0][nm] = lepton[z].e();
      theMuons[1][nm] = lepton[z].px();
      theMuons[2][nm] = lepton[z].py();
      theMuons[3][nm] = lepton[z].pz();
      //cout << "muon " << nm << "\t" << lepton[z].e() << "\t" << lepton[z].px() << "\t" << lepton[z].py() << "\t" << lepton[z].pz() << endl;
      nm++;
    }
    if(lepton_id[z] == -13) {
      theantiMuons[0][nam] = lepton[z].e();
      theantiMuons[1][nam] = lepton[z].px();
      theantiMuons[2][nam] = lepton[z].py();
      theantiMuons[3][nam] = lepton[z].pz();
      //cout << "anti-muon " << nam << "\t" << lepton[z].e() << "\t" << lepton[z].px() << "\t" << lepton[z].py() << "\t" << lepton[z].pz() << endl;
      nam++;
    }
    if(lepton_id[z] == 11) {
      theElectrons[0][ne] = lepton[z].e();
      theElectrons[1][ne] = lepton[z].px();
      theElectrons[2][ne] = lepton[z].py();
      theElectrons[3][ne] = lepton[z].pz();
      //cout << "electron " << ne << "\t" << lepton[z].e() << "\t" << lepton[z].px() << "\t" << lepton[z].py() << "\t" << lepton[z].pz() << endl;
      ne++;
    }
      
    if(lepton_id[z] == -11) {
      thePositrons[0][nae] = lepton[z].e();
      thePositrons[1][nae] = lepton[z].px();
      thePositrons[2][nae] = lepton[z].py();
      thePositrons[3][nae] = lepton[z].pz();
      //cout << "anti-electron " << nae << "\t" << lepton[z].e() << "\t" << lepton[z].px() << "\t" << lepton[z].py() << "\t" << lepton[z].pz() << endl;
      nae++;
    }
  }

  //fill in the ETmiss vector
  theETmiss[0] = pmiss.e();
  theETmiss[1] = pmiss.px();
  theETmiss[2] = pmiss.py();
  theETmiss[3] = pmiss.pz();

  //find the number present for each object
  numElectrons = ne;
  numPositrons = nae;
  numMuons = nm;
  numantiMuons = nam;
  numPhotons = photon.size();
  numJets = nj;
  numbJets = nb;
  numHiggses = nh;

  
  // cout << "numJets = " << numJets << endl;
  
  /* 
   * APPLY BASIC CUTS
   */
  if(onfly_) { 
    if(BasicCutsPass(numparticles, objects, jet, photon, lepton, lepton_id, pmiss, cut_eta, cut_pt_part, cut_pt_jet, cut_eta_jet, cut_pt_muon, cut_eta_muon, cut_pt_electron, cut_eta_electron)==true) {
      //cout << "Passed " << endl;     
            
      /*
     * IF THE EVENT HAS PASSED THE BASIC CUTS:
     * Increment event number and fill root trees
     */
      nevpass++;
      nevpass_weighted+=evweight;
      Data->Fill();
    }
  }
  else { //no on-the-fly analysis
    if(fatjets_) {
      pair<vector<PseudoJet>, vector<double> > fatjets_tau = FatJetAnalysis(numparticles, objects, jet, photon, lepton, lepton_id, pmiss, cut_eta, cut_pt_part, cut_pt_fatjet_, cut_eta_fatjet_, cut_pt_muon, cut_eta_muon, cut_pt_electron, cut_eta_electron);

      vector<PseudoJet> fatjets = fatjets_tau.first;
      vector<double> tau21 = fatjets_tau.second;
      int nj = 0, nb = 0;
      for(int z = 0; z < fatjets.size(); z++) {
	theFatJets[0][nj] = fatjets[z].e();
	theFatJets[1][nj] = fatjets[z].px();
	theFatJets[2][nj] = fatjets[z].py();
	theFatJets[3][nj] = fatjets[z].pz();
	tau21FatJets[nj] = tau21[z];
      }
      numFatJets = fatjets.size();
    }
    
    nevpass++;
    nevpass_weighted+=evweight;
    Data->Fill();
  }
 

  //clear particle vectors for next use
  particlesToCluster_.clear();

}

/***********************************************************************
 This is where the main analysis function ends
************************************************************************/

ClassDescription<HwSimFat> HwSimFat::initHwSimFat;
// Definition of the static class description member.

//The Following provides INTERFACES to the various settings of the analysis
void HwSimFat::Init() {

  static ClassDocumentation<HwSimFat> documentation
    ("The HwSimFat class has been created for the simple object reconstruction");
  
  static Switch<HwSimFat,int> interfaceJetAlgorithm
    ("JetAlgorithm",
     "Determines the jet algorithm for finding jets in parton-jet ",
     &HwSimFat::jetAlgorithm_, -1, false, false);
  static SwitchOption AntiKt
    (interfaceJetAlgorithm,
     "AntiKt",
     "The anti-kt jet algorithm.",
     -1);
  static SwitchOption CambridgeAachen
    (interfaceJetAlgorithm,
     "CambridgeAachen",
     "The Cambridge-Aachen jet algorithm.",
     0);
  static SwitchOption Kt
    (interfaceJetAlgorithm,
     "Kt",
     "The Kt jet algorithm.",
     1);

    static Switch<HwSimFat,bool> interfaceSaveObjects
    ("SaveObjects",
     "Determines whether to save all objects to root file or not",
     &HwSimFat::saveObjects_, 0, false, false);
  static SwitchOption SaveObjectsNo
    (interfaceSaveObjects,
     "No",
     "Do not save all objects to root file.",
     0);
  static SwitchOption SaveObjectsYes
    (interfaceSaveObjects,
     "Yes",
     "Save objects to root file.",
     1);

  static Switch<HwSimFat,bool> interfaceSaveReconstructed
    ("SaveReconstructed",
     "Determines whether to save reconstructed particles or not.",
     &HwSimFat::saveReconstructed_, 1, false, false);
  static SwitchOption SaveReconstructedNo
    (interfaceSaveReconstructed,
     "No",
     "Do not save reconstructed particles to root file.",
     0);
  static SwitchOption SaveReconstructedYes
    (interfaceSaveReconstructed,
     "Yes",
     "Save reconstructed particles to root file.",
     1);

    static Switch<HwSimFat,bool> interfaceCharmTagging
    ("CharmTagging",
     "Use any charm quarks present at parton level to perform c-tagging of jets.",
     &HwSimFat::ctagging_, 0, false, false);
  static SwitchOption CharmTaggingNo
    (interfaceCharmTagging,
     "No",
     "Do not perform c-tagging.",
     0);
  static SwitchOption CharmTaggingYes
    (interfaceCharmTagging,
     "Yes",
     "Perform c-tagging.",
     1);

  static Switch<HwSimFat,int> interfaceBTaggingMethod
    ("BTaggingMethod",
     "Defines the b-tagging method. ",
     &HwSimFat::btaggingmethod_, 0, false, false);
  static SwitchOption BTaggingMethodDefault
    (interfaceBTaggingMethod,
     "Default",
     "B-tag jets that contain b-hadrons.",
     0);
  static SwitchOption BTaggingMethodDefaultBHadronDR
    (interfaceBTaggingMethod,
     "BHadronDR",
     "Use the distance of jets to b-hadrons.",
     1);
  static SwitchOption BTaggingMethodDefaultBQuarkDR
    (interfaceBTaggingMethod,
     "BQuarkDR",
     "Use the distance of jets to b-quarks.",
     2);

   static Switch<HwSimFat,bool> interfaceOnTheFlyAnalysis
    ("OnTheFlyAnalysis",
     "Perform on-the-fly analysis or not.",
     &HwSimFat::onfly_, 0, false, false);
  static SwitchOption OnTheFlyAnalysisNo
    (interfaceOnTheFlyAnalysis,
     "No",
     "Do not perform on-the-fly analysis.",
     0);
  static SwitchOption OnTheFlyAnalysisYes
    (interfaceOnTheFlyAnalysis,
     "Yes",
     "Perform on-the-fly analysis.",
     1);

  static Switch<HwSimFat,bool> interfaceFatJets
    ("FatJets",
     "Store fat jets",
     &HwSimFat::fatjets_, 0, false, false);
  static SwitchOption FatJetsNo
    (interfaceFatJets,
     "No",
     "Do not do fat jet finding.",
     0);
  static SwitchOption FatJetsYes
    (interfaceFatJets,
     "Yes",
     "Do fat jet finding.",
     1);

    static Switch<HwSimFat,bool> interfacePruneFatJets
    ("PruneFatJets",
     "Prune the stored fat jets",
     &HwSimFat::pruneFat_, 0, false, false);
  static SwitchOption PruneFatNo
    (interfacePruneFatJets,
     "No",
     "Do not prune fat jets.",
     0);
  static SwitchOption PruneFatYes
    (interfacePruneFatJets,
     "Yes",
     "Prune the fat jets.",
     1);
  

    static Switch<HwSimFat,bool> interfaceSaveOptionalWeights
    ("SaveOptionalWeights",
     "Save optional weights or not.",
     &HwSimFat::saveoptionalweights_, 0, false, false);
  static SwitchOption SaveOptionalWeightsNo
    (interfaceSaveOptionalWeights,
     "No",
     "Do not save optional weights.",
     0);
  static SwitchOption SaveOptionalWeightsYes
    (interfaceSaveOptionalWeights,
     "Yes",
     "Save optional weights",
     1);

  
  static Switch<HwSimFat,bool> interfaceApplyMuonIsolation
    ("ApplyMuonIsolation",
     "Apply Muon Isolation or not.",
     &HwSimFat::muonisolation_, 0, false, false);
  static SwitchOption ApplyMuonIsolationNo
    (interfaceApplyMuonIsolation,
     "No",
     "Do not apply muon isolation.",
     0);
  static SwitchOption ApplyMuonIsolationYes
    (interfaceApplyMuonIsolation,
     "Yes",
     "apply muon isolation.",
     1);

  
  static Switch<HwSimFat,bool> interfaceApplyElectronIsolation
    ("ApplyElectronIsolation",
     "Apply Electron Isolation or not.",
     &HwSimFat::electronisolation_, 0, false, false);
  static SwitchOption ApplyElectronIsolationNo
    (interfaceApplyElectronIsolation,
     "No",
     "Do not apply electron isolation.",
     0);
  static SwitchOption ApplyElectronIsolationYes
    (interfaceApplyElectronIsolation,
     "Yes",
     "apply electron isolation.",
     1);

  static Parameter<HwSimFat, double> interfaceElectronIsoR
    ("ElectronIsoR",
     "Electron Isolation radius R",
     &HwSimFat::electronIsoR_, 0.2, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceMuonIsoR
    ("MuonIsoR",
     "Muon Isolation radius R",
     &HwSimFat::muonIsoR_, 0.2, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceElectronIso
    ("ElectronIsoFrac",
     "Electron Isolation radius R",
     &HwSimFat::electronIsoFrac_, 0.1, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceMuonIsoFrac
    ("MuonIsoFrac",
     "Muon Isolation radius R",
     &HwSimFat::muonIsoFrac_, 0.1, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfacePTCutParticles
    ("PTCutParticles",
     "Minimum particle pT to be considered.",
     &HwSimFat::cut_pt_part_, 0.1, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceEtaCutParticles
    ("EtaCutParticles",
     "Minimum particle pT to be considered.",
     &HwSimFat::cut_eta_, 5.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfacePTCutJets
    ("PTCutJets",
     "Minimum jet pT to be considered.",
     &HwSimFat::cut_pt_jet_, 20.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceEtaCutJets
    ("EtaCutJets",
     "Minimum jet pT to be considered.",
     &HwSimFat::cut_eta_jet_, 5.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfacePTCutFatJets
    ("PTCutFatJets",
     "Minimum jet pT to be considered for fat jets.",
     &HwSimFat::cut_pt_fatjet_, 200.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceEtaCutFatJets
    ("EtaCutFatJets",
     "Minimum jet pT to be considered for fat jets.",
     &HwSimFat::cut_eta_fatjet_, 5.0, 0.0, 100000.0,
     false, false, Interface::limited);
    

    static Parameter<HwSimFat, double> interfacePTCutElectron
    ("PTCutElectron",
     "Minimum electron pT to be considered.",
     &HwSimFat::cut_pt_electron_, 10.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceEtaCutElectron
    ("EtaCutElectron",
     "Minimum electron pT to be considered.",
     &HwSimFat::cut_eta_electron_, 3.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfacePTCutMuon
      ("PTCutMuon",
     "Minimum muon pT to be considered.",
     &HwSimFat::cut_pt_muon_, 10.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceEtaCutMuon
    ("EtaCutMuon",
     "Minimum muon pT to be considered.",
     &HwSimFat::cut_eta_muon_, 3.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfacePTCutPhoton
      ("PTCutPhoton",
     "Minimum photon pT to be considered.",
     &HwSimFat::cut_pt_photon_, 20.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceEtaCutPhoton
    ("EtaCutPhoton",
     "Minimum photon pT to be considered.",
     &HwSimFat::cut_eta_photon_, 5.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceRParameter
    ("RParameter",
     "Jet algorithm R-parameter",
     &HwSimFat::R_, 0.4, 0.0, 100000.0,
     false, false, Interface::limited);
    
    static Parameter<HwSimFat, double> interfaceRfatParameter
    ("RFatParameter",
     "Jet algorithm R-parameter for the case of fat jets",
     &HwSimFat::FatR_, 0.8, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceFatZcut
    ("FatZcut",
     "Pruning z-cut for the case of fat jets",
     &HwSimFat::zcut_, 0.1, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, double> interfaceFatRcutFactor
    ("FatRcutFactor",
     "Pruning z-cut for the case of fat jets",
     &HwSimFat::rcut_factor_, 0.5, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimFat, string> interfaceOutpuLocation
    ("OutputLocation",
     "The output location for the root files",
     &HwSimFat::rootoutlocation_, "", false, false);
}


/***********************************************************************
 This function is executed at the START of the analysis
************************************************************************/
void HwSimFat::doinitrun() {
  //initialize Analysis handler
  AnalysisHandler::doinitrun();

  //set the random seed
  rnd.SetSeed(1012312);
 
  //output to screen 
  std::cout << "HwSimFat Loaded." << endl;
  
  //reset number counters
  numevents_=0.;
  nevpass = 0;
  nevpass_weighted = 0;

  //prepare root tree
  prepareroottree();
  if(saveReconstructed_ == true) {
    cout << "Jet Algorithm: " << jetAlgorithm_ << " R = " << R_ << endl;
    cout << "Cuts: " << cut_eta_ << " " << cut_pt_part_ << " " << cut_pt_jet_ << " " << cut_eta_jet_ << " " << cut_pt_electron_ << " " << cut_eta_electron_ << " " << cut_pt_muon_ << " " << cut_eta_muon_ << " " << cut_pt_photon_ << " " << cut_eta_photon_ << endl;
  } else {
    cout << "Not saving any reconstructed objects!" << endl;
  }
  if(fatjets_ == true) {
    cout << "Saving fat jets with FatR=" << FatR_ << endl;
  }
  if(pruneFat_ == true) {
    cout << "If fat jets are saved, they are also pruned and their tau21 is calculated" << endl;
  }

}

/***********************************************************************
 This function is executed at the END of the analysis
************************************************************************/
void HwSimFat::dofinish() {
  //finish Analysis handler
  AnalysisHandler::dofinish();
  
  //write root tree
  writeroottree();
  
  /* 
   * PRINT CUTS AND EVENTS
   * THAT PASSED
   */
  
  cout << "Number of events that pass basic cuts: " << nevpass << endl;
  cout << "Weight of events that pass basic cuts: " << nevpass_weighted << endl;
  
}

/***********************************************************************
 This function prepares the root tree for writing out event files
************************************************************************/
int HwSimFat::prepareroottree() {
  std::cout << "Preparing Root Tree for event files" << endl;
  
  fnameroot = "";
  fnameroot = rootoutlocation_ + generator()->filename() + ".root";
  dat = new TFile(fnameroot.c_str(), "RECREATE");
  
  Data = new TTree ("Data", "Data Tree");
  //variables to fill in the .root file

  if(saveoptionalweights_ == true) {
    Data->Branch("theOptWeights","vector<double>",&theOptWeights);
    Data->Branch("theOptWeightsNames","vector<string>",&theOptWeightsNames);
  }


  if(saveObjects_ == true) { 
    Data->Branch("numparticles", &numparticles, "numparticles/I");
    Data->Branch("objects", &objects, "objects[8][10000]/D");
  }

  if(saveReconstructed_ == true) {
    Data->Branch("thebJets", &thebJets, "thebJets[4][100]/D");
    Data->Branch("numbJets", &numbJets, "numbJets/I");
    
    Data->Branch("theHiggses", &theHiggses, "theHiggses[4][100]/D");
    Data->Branch("numHiggses", &numHiggses, "numHiggses/I");
    
    Data->Branch("theJets", &theJets, "theJets[4][100]/D");
    Data->Branch("numJets", &numJets, "numJets/I");

    Data->Branch("cTag", &cTag, "cTag[100]/D");
    
    Data->Branch("thePhotons", &thePhotons, "thePhotons[4][100]/D");
    Data->Branch("numPhotons", &numPhotons, "numPhotons/I");
    
    Data->Branch("theMuons", &theMuons, "theMuons[4][100]/D");
    Data->Branch("numMuons", &numMuons, "numMuons/I");
    
    Data->Branch("theantiMuons", &theantiMuons, "theantiMuons[4][100]/D");
    Data->Branch("numantiMuons", &numantiMuons, "numantiMuons/I");
    
    Data->Branch("thePositrons", &thePositrons, "thePositrons[4][100]/D");
    Data->Branch("numPositrons", &numPositrons, "numPositrons/I");
    
    Data->Branch("theElectrons", &theElectrons, "theElectrons[4]][100]/D");
    Data->Branch("numElectrons", &numElectrons, "numElectrons/I");
    
    
    Data->Branch("theETmiss", &theETmiss, "theETmiss[4]/D");

    if(fatjets_ == true) {
      Data->Branch("theFatJets", &theFatJets, "theFatJets[4][100]/D");
      Data->Branch("numFatJets", &numFatJets, "numFatJets/I");
      Data->Branch("tau21FatJets", &tau21FatJets, "tau21FatJets[100]/D");

    }
  }
    
  Data->Branch("evweight", &evweight, "evweight/D");

  return 1;
}

/***********************************************************************
 This function writes the root event file
************************************************************************/
int HwSimFat::writeroottree() { 

  Data->GetCurrentFile();
  Data->Write();
  dat->Close();
  cout << "A root tree has been written to the file:" << fnameroot << endl;
  return 1;
}

#include "HwSimGetJet.h"

#include "HwSimAnalysis.h"

