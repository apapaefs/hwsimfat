// -*- C++ -*-
//
// MC4BSMAnalysis.h is a part of Herwig++ - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2007 The Herwig Collaboration
//
// Herwig++ is licenced under version 2 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
#ifndef HERWIG_HwSimFat_H
#define HERWIG_HwSimFat_H
//
// This is the declaration of the HwSim class.
//

#include "ThePEG/Repository/CurrentGenerator.h"
#include "ThePEG/Handlers/AnalysisHandler.h"
#include "ThePEG/Config/Pointers.h"
#include "Herwig/Utilities/Histogram.h"

#include <cmath>
#include <fstream>

#include "TTree.h"
#include "TFile.h"
#include "TStyle.h"
#include "TH1F.h"
#include "TRandom3.h"
#include "TVector.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"
#include "TString.h"
#include "TMath.h"
#include "TH2.h"
#include "THStack.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"


//Fastjet headers
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include <fastjet/tools/JHTopTagger.hh>
#include <fastjet/Selector.hh>
#include "fastjet/tools/Pruner.hh"

#include "fastjet/contrib/Nsubjettiness.hh" // In external code, this should be fastjet/contrib/Nsubjettiness.hh
#include "fastjet/contrib/Njettiness.hh"
#include "fastjet/contrib/NjettinessPlugin.hh"
#include "fastjet/contrib/XConePlugin.hh"

//Boost headers
#include <boost/algorithm/string.hpp>
#include <boost/tuple/tuple.hpp>
//costum headers
#include "complex_d.h"

namespace Herwig {

class HwSimFat;

}

namespace ThePEG {

ThePEG_DECLARE_POINTERS(Herwig::HwSimFat,HwSimFatPtr);

}


namespace Herwig {

  using namespace ROOT;
  using namespace ThePEG;
  using namespace std;
  using namespace fastjet;
  
/**
 * The HwSimFat class is designed to perform some simple analysis of
 * gauge boson, W and Z, distributions in hadron-hadron collisions. The main 
 * distriubtions are the transverse momentum and rapidities of the gauge bosons
 * which are of physical interest, and the azimuthal angle distribution for
 * testing purposes.
 *
 * @see \ref HwSimFatInterfaces "The interfaces"
 * defined for HwSimFat.
 */
class HwSimFat: public AnalysisHandler {

public:

  /**
   * The default constructor.
   */
  inline HwSimFat();


  /** @name Functions used by the persistent I/O system. */
  //@{
  /**
   * Function used to write out object persistently.
   * @param os the persistent output stream written to.
   */
  void persistentOutput(PersistentOStream & os) const;

  /**
   * Function used to read in object persistently.
   * @param is the persistent input stream read from.
   * @param version the version number of the object when written.
   */
  void persistentInput(PersistentIStream & is, int version);
  //@}
  
  /** @name Virtual functions required by the AnalysisHandler class. */
  //@{
  /**
   * Analyze a given Event. Note that a fully generated event
   * may be presented several times, if it has been manipulated in
   * between. The default version of this function will call transform
   * to make a lorentz transformation of the whole event, then extract
   * all final state particles and call analyze(tPVector) of this
   * analysis object and those of all associated analysis objects. The
   * default version will not, however, do anything on events which
   * have not been fully generated, or have been manipulated in any
   * way.
   * @param event pointer to the Event to be analyzed.
   * @param ieve the event number.
   * @param loop the number of times this event has been presented.
   * If negative the event is now fully generated.
   * @param state a number different from zero if the event has been
   * manipulated in some way since it was last presented.
   */
  virtual void analyze(tEventPtr event, long ieve, int loop, int state);

 
  //@}


public:

  /**
   * The standard Init function used to initialize the interfaces.
   * Called exactly once for each class by the class description system
   * before the main function starts or
   * when this class is dynamically loaded.
   */
  static void Init();



protected:

  /** @name Clone Methods. */
  //@{
  /**
   * Make a simple clone of this object.
   * @return a pointer to the new object.
   */
  inline virtual IBPtr clone() const;

  /** Make a clone of this object, possibly modifying the cloned object
   * to make it sane.
   * @return a pointer to the new object.
   */
  inline virtual IBPtr fullclone() const;
  //@}



protected:
  inline virtual void doinitrun();
  /** @name Standard Interfaced functions. */
  //@{
  /**
   * Finalize this object. Called in the run phase just after a
   * run has ended. Used eg. to write out statistics.
   */
  virtual void dofinish();
  //@}

private:

  /**
   * The static object used to initialize the description of this class.
   * Indicates that this is a concrete class with persistent data.
   */
  static ClassDescription<HwSimFat> initHwSimFat;

  /**
   * The assignment operator is private and must never be called.
   * In fact, it should not even be implemented.
   */
  HwSimFat & operator=(const HwSimFat &);
  

  /** 
   *  Root Trees for writing out event files 
   **/
  TTree * Data;
  TFile * dat;

  /**
   * Variables used for storage in root even files
   */

  //total number of particles in an event
  int numparticles;

  //total number of undecayed Higgses in event
  int numHiggses;

  //total number of jets in an event (non-btagged)
  int numJets;

  //total number of fat jets in an event
  int numFatJets;

  //total number of jets in an event
  int numbJets;

  //total number of photons in an event
  int numPhotons;

  //total number of Electrons in an event
  int numElectrons;

  //total number of Positrons in an event
  int numPositrons;

  //total number of Muons in an event
  int numMuons;

  //total number of Muons in an event
  int numantiMuons;
  
  /** particle information in the order: 
   * 4 momenta (E,x,y,z), id, boolean daughter of Higgs, boolean daughter of b-quark, unused
   **/
  double objects[8][10000];


  /* 
   * fat jet pruning parameters:
   */
  double zcut_;
  double rcut_factor_;

  /* 
   * whether to perform pruning on the fat jets
   */
  bool pruneFat_;
  

  /* 
   * the event weight
   */ 
  double evweight;

  /* The vector of jets */

  double theJets[4][100];

  /* The vector of fat jets */
  
  double theFatJets[4][100];

  /* the tau21 for the fat jets */
  
  double tau21FatJets[100];

   /* c-tagging for light jets */

  double cTag[100];

  /* the optional weight values */

  std::vector<double> *theOptWeights;

  /* the optional weight names */
  
  std::vector<string> *theOptWeightsNames;


  /* The vector of b-jets */

  double thebJets[4][100];

  /* The vector of photons */

  double thePhotons[4][100];

  /* The vector of muons */

  double theMuons[4][100];

  /* The vector of electrons */

  double theElectrons[4][100];

   /* The vector of anti-muons */

  double theantiMuons[4][100];

  /* The vector of positrons */

  double thePositrons[4][100];

  
  /* The vector of Higgses (if undecayed) */

  double theHiggses[4][100];


  /* The missing energy four-vector */

  double theETmiss[4];

  /* 
   * Pointer variables for the isParent() function
   */ 

  tPPtr test_parent_, test_current_, test_current2_;
 
  /* 
   * total number of events.
   */ 
  double numevents_;

  /*
   * location of the root output file
   */ 
  string rootoutlocation_;

private:

  /**
   *  ONLY the final-state particles that are supposed to go for jet clustering.
   */
  tPVector particlesToCluster_;
  
  /*
   * The jet algorithm used for parton-jet matching in the HH procedure.
   */
  int jetAlgorithm_;
  
  /* global particle cuts */
  double cut_eta_;
  double cut_pt_part_;
  
  /* jet cuts */
  double cut_pt_jet_; //pt cut for jets
  double cut_eta_jet_; //pseudo-rapidity cut for jets

  /* fat jet cuts */
  double cut_pt_fatjet_; //pt cut for fat jets
  double cut_eta_fatjet_; //pseudo-rapidity cut for fat jets

  /* lepton cuts */
  double cut_eta_electron_; // pseudo-rapidity cut for electrons
  double cut_pt_electron_; // pt cut for electrons

  double cut_eta_muon_; // pseudo-rapidity cut for muons
  double cut_pt_muon_; // pt cut for muons

  /* photon cuts */
  double cut_pt_photon_; //pt cut for jets
  double cut_eta_photon_; //pseudo-rapidity cut for jets

  /* jet-flavour association */
  double fl_DeltaR_;
  double fl_PartonPTMin_;
  double fl_PartonEtaMax_;

  /*
   * jet algorithm radius parameter
   */
  double R_;

  /*
   * jet algorithm radius parameter in the case of fat jets
   */
  double FatR_;

    /*
   * electron isolation radius
   */
  double electronIsoR_;  

  /*
   * muon isolation radius
   */
  double muonIsoR_;

  /*
   * electron isolation fraction
   */
  double electronIsoFrac_;  
  
  /*
   * muon isolation fraction
   */
  double muonIsoFrac_;  
  
  /*
   * Save all the final state objects or not?
   */
  bool saveObjects_;

  /*
   * Save reconstructed objects or not?
   */
  bool saveReconstructed_;

  /*
   * Apply electron isolation criteria or not?
   */
  bool electronisolation_;

  /*
   * Apply muon isolation criteria or not?
   */
  bool muonisolation_;

    /*
   * Switch for charm tagging of jets. 
   */
  bool ctagging_;

  /*
   * Switch for b-tagging method.
   */
  int btaggingmethod_;
  

  /*
   * Switch for performing on-the-fly analysis
   */

  bool onfly_;

  /*
   * Switch for saving fat jets
   */

  bool fatjets_;

  /*
   * Save optional weights?
   */

  bool saveoptionalweights_;
  
  /*
   * Vector of charms
   */
  vector<tPPtr> charms;
 
 
  /* use the undecayed B-hadrons for b-tagging */
  
  bool btag_hadrons(fastjet::PseudoJet jet);

  /* delta R distance between two fastjets */
  
  double deltaR(fastjet::PseudoJet p1, fastjet::PseudoJet p2);

  /* dot product between two fastjets */

  double dot(fastjet::PseudoJet p1, fastjet::PseudoJet p2);
  
   /* 
   * A function that prints a vector of Lorentz5Momenta in a fancy way
   */
  void printMomVec(vector<Lorentz5Momentum> momVec);

  /* 
   * Is particle A a parent of particle B?
   */ 
  bool isParent(ThePEG::tPPtr A, ThePEG::tPPtr B);

  /* 
   * Is particle A a daughter of a hadron?
   */ 
  
  bool isParentAHadron(ThePEG::tPPtr A);

  /* 
   * Is particle B a daughter of a particle of ID IDA?
   */ 
  bool isParentID(int IDA, ThePEG::tPPtr B) ;

  ParticleVector getChildren( ThePEG::tPPtr B );

  //prepare and write root tree event files
  int prepareroottree();
  int writeroottree();
  
  //root event filename
  string fnameroot;

  /* 
   * Analysis to store only events with basic cuts and functions related to it
   */ 
  bool BasicCutsPass(int numparticles, double objects[8][10000], vector<fastjet::PseudoJet> jet, vector<fastjet::PseudoJet> photon, vector<fastjet::PseudoJet> lepton, vector<int> lepton_id, fastjet::PseudoJet pmiss, double cut_eta, double cut_pt_part, double cut_pt_jet, double cut_eta_jet, double cut_pt_muon, double cut_eta_muon, double cut_pt_electron, double cut_eta_electron);


  /* 
   * Analysis to return fat jets in the event
   */
  pair<vector<fastjet::PseudoJet>, vector<double> > FatJetAnalysis(int numparticles, double objects[8][10000], vector<fastjet::PseudoJet> jet, vector<fastjet::PseudoJet> photon, vector<fastjet::PseudoJet> lepton, vector<int> lepton_id, fastjet::PseudoJet pmiss, double cut_eta, double cut_pt_part, double cut_pt_jet, double cut_eta_jet, double cut_pt_muon, double cut_eta_muon, double cut_pt_electron, double cut_eta_electron);
  
  //bool BasicCutsPass(int numparticles, double objects[8][10000]);

  /*
   * the number of events that pass
   */ 
  int nevpass;

  /*
   * the number of weights that pass
   */
  double nevpass_weighted;

  /* Set up random number
   * generator using ROOT
   */
  
  TRandom3 rnd;

};

}

#include "ThePEG/Utilities/ClassTraits.h"

namespace ThePEG {

/** @cond TRAITSPECIALIZATIONS */

/** This template specialization informs ThePEG about the
 *  base classes of HwSim. */
template <>
struct BaseClassTrait<Herwig::HwSimFat,1> {
  /** Typedef of the first base class of HwSim. */
  typedef AnalysisHandler NthBase;
};

/** This template specialization informs ThePEG about the name of
 *  the HwSim class and the shared object where it is defined. */
template <>
struct ClassTraits<Herwig::HwSimFat>
  : public ClassTraitsBase<Herwig::HwSimFat> {
  /** Return a platform-independent class name */
  static string className() { return "Herwig::HwSimFat"; }
  /** Return the name(s) of the shared library (or libraries) be loaded to get
   *  access to the HwSim class and any other class on which it depends
   *  (except the base class). */
  static string library() { return "HwSimFat.so"; }
};

/** @endcond */

}

#endif /* HERWIG_HwSim_H */
