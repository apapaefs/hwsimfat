/***********************************************************************
WARNING: You do not need to touch anything below this!
************************************************************************/
void HwSimFat::printMomVec(vector<Lorentz5Momentum> momVec) {
  cout << "\n\n";

  // Label columns.
  printf("%5s %9s %9s %9s %9s %9s %9s %9s %9s %9s\n",
	 "jet #",
	 "px","py","pz","E",
	 "eta","phi","pt","et","mass");

  // Print out the details for each jet
  for (unsigned int ixx=0; ixx<momVec.size(); ixx++) {
    printf("%5u %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f\n",
	   ixx,
	   double(momVec[ixx].x()/GeV),double(momVec[ixx].y()/GeV),
	   double(momVec[ixx].z()/GeV),double(momVec[ixx].t()/GeV),
	   double(momVec[ixx].eta())  ,double(momVec[ixx].phi()),
	   double(momVec[ixx].perp()/GeV),
	   double(momVec[ixx].et()/GeV),
	   double(momVec[ixx].m()/GeV));
  }

}

/* 
 * Check if the particle A is the parent of particle B
 * at any point in the event record.
 * Return true if so, false otherwise.
 */ 
bool HwSimFat::isParent(ThePEG::tPPtr A, ThePEG::tPPtr B) { 
  
  //boolean for return value
  bool istrue(false);

  //check B and A momenta
  Lorentz5Momentum Bmom((B->final())->momentum());
  Lorentz5Momentum Amom((A->final())->momentum());

  //If either momenta are zero then return false 
  if((Amom.e())/GeV == 0 || (Bmom.e())/GeV == 0) { return false; }
  
  //define the two currently tested particles
  test_current_ = B;
  test_current2_ = B;
  Bmom = test_current_->momentum();  
  
  //loop over the parents of the particle
  while(test_current_->parents().size() != 0) { 
    /*if the currently tested particle has a momentum equal to that of A,
     * the parent has been found, return true*/
    if( test_current_->momentum() == Amom ) { istrue = true;}
    if( test_current2_->momentum() == Amom ) { istrue = true;}
    
    //If the size of the parents is not zero, then redefine test_current_ to be the first parent
    if(test_current_->parents().size() != 0) { 
      test_current_ = test_current_->parents()[0]; 
    }
    //if the size of the parents is 2 then redefine test_current2_ to be the second parent
    if(test_current_->parents().size() == 2) { 
      test_current2_ = test_current_->parents()[1]; 
    }
  }//end of while loop

  //return whatever the boolean has been set to
  return istrue;
}

//is a particle of ID a parent of current particle B?
inline bool HwSimFat::isParentID(int IDA, ThePEG::tPPtr B) { 
  //cout << "isParent" << endl;
  bool istrue = 0;
  Lorentz5Momentum Parent5Mom, Amom, Bmom; 
  // cout << "A id = " << A->id() << " B id = " << B->id() << endl;

  Bmom = (B->final())->momentum();
  /*cout << "A(x,y,z) = " <<  Amom.x()/GeV << " " << Amom.y()/GeV << " " << Amom.z()/GeV << endl;
  cout << "B(x,y,z) = " <<  Bmom.x()/GeV << " " << Bmom.y()/GeV << " " << Bmom.z()/GeV << endl;
  */

  if((Bmom.e())/GeV == 0) { cout << "error: particle B has zero energy!" << endl; exit(1); }

  //ThePEG::tPPtr Boriginal = (B)->original();
  //  cout << "id b = " << B->final->id() << endl;
  
  ThePEG::tPPtr test_parent, test_current, test_current2;
  test_current = B;
  test_current2 = B;
  Bmom = test_current->momentum();
  
  
  while(test_current->parents().size() != 0) { 

    if( test_current->id() == IDA ) { istrue = 1;}
    if( test_current2->id() == IDA ) { istrue = 1;}


    if(test_current->parents().size() != 0) { 
      test_current = test_current->parents()[0]; 
    }
    if(test_current->parents().size() == 2) { 
      test_current2 = test_current->parents()[1]; 
    } 
  }

  
  return istrue;
}

//did this particle come from a hadron?
inline bool HwSimFat::isParentAHadron(ThePEG::tPPtr B) { 
  //cout << "isParent" << endl;
  bool istrue = 0;
  Lorentz5Momentum Parent5Mom, Amom, Bmom; 
  // cout << "A id = " << A->id() << " B id = " << B->id() << endl;

  Bmom = (B->final())->momentum();
  /*cout << "A(x,y,z) = " <<  Amom.x()/GeV << " " << Amom.y()/GeV << " " << Amom.z()/GeV << endl;
  cout << "B(x,y,z) = " <<  Bmom.x()/GeV << " " << Bmom.y()/GeV << " " << Bmom.z()/GeV << endl;
  */

  if((Bmom.e())/GeV == 0) { cout << "error: particle B has zero energy!" << endl; exit(1); }

  //ThePEG::tPPtr Boriginal = (B)->original();
  //  cout << "id b = " << B->final->id() << endl;
  
  ThePEG::tPPtr test_parent, test_current, test_current2;
  test_current = B;
  test_current2 = B;
  Bmom = test_current->momentum();
  
  
  while(test_current->parents().size() != 0) { 

    if( fabs(test_current->id()) > 80 && fabs(test_current->id()) != 2212 && fabs(test_current->id()) != 99927 && fabs(test_current->id()) != 99926) {/* cout << fabs(test_current->id()) << endl; */ istrue = 1;}
    if( fabs(test_current->id()) > 80 && fabs(test_current->id()) != 2212 && fabs(test_current->id()) != 99927 && fabs(test_current->id()) != 99926) {/*cout << fabs(test_current->id()) << endl;*/ istrue = 1;}


    if(test_current->parents().size() != 0) { 
      test_current = test_current->parents()[0]; 
    }
    if(test_current->parents().size() == 2) { 
      test_current2 = test_current->parents()[1]; 
    } 
  }
  return istrue;
}

inline ParticleVector getChildren( tPPtr B ) { 
  cout << "id = " << B->id() << endl;
  
  ParticleVector theChildren;
  ThePEG::tPPtr test_current = B; 
  while(test_current->children().size() == 1) { 
    cout << "id = " << test_current->id() << endl;
    test_current = test_current->children()[0];
  }
  cout << "children size " << test_current->children().size() << endl; 
  theChildren.resize(test_current->children().size());
  for(int f = 0; f < test_current->children().size(); f++) { 
    theChildren[f] = test_current->children()[f];
  }
  return theChildren;
}
double HwSimFat::deltaR(fastjet::PseudoJet p1, fastjet::PseudoJet p2) { 
  double dphi_tmp; 

  dphi_tmp = p2.phi() - p1.phi();
  if(dphi_tmp > M_PI) 
    dphi_tmp = 2 * M_PI - dphi_tmp;
  else if( dphi_tmp < - M_PI)  
    dphi_tmp = 2 * M_PI + dphi_tmp;
  
  return sqrt(sqr(p1.eta() - p2.eta()) + sqr(dphi_tmp));
}
double HwSimFat::dot(fastjet::PseudoJet p1, fastjet::PseudoJet p2) {
  return (p1.e() * p2.e() - p1.px() * p2.px() - p1.py() * p2.py() - p1.pz() * p2.pz() );
}

bool HwSimFat::btag_hadrons(fastjet::PseudoJet jet) {
  bool btagged(false);
  /* search constintuents of jet for b-mesons */
  int bhadronid[105] = {5122, -5122, 15122, -15122, 5124, -5124, 5334, -5334, 5114, -5114, 5214, -5214, 5224, -5224, 5112, -5112, 5212, -5212, 5222, -5222, 15322, -15322, 15312, -15312, 15324, -15324, 15314, -15314, 5314, -5314, 5324, -5324, 5132, -5132, 5232, -5232, 5312, -5312, 5322, -5322, 551, 10555, 100551, 200551, 553, 557, 555, 100555, 200555, 20523, -20523, 20513, -20513, 20543, -20543, 20533, -20533, 511, 521, -511, -521, 531, -531, 541, -541, 513, 523, -513, -523, 533, -533, 543, -543, 10513, 10523, -10513, -10523, 10533, -10533, 10543, -10543, 10511, 10521, -10511, -10521, 10531, -10531, 10541, -10541, 20513, 20523, -20513, -20523, 20533, -20533, 20543, -20543, 515, 525, -515, -525, 535, -535, 545, -545};
  for(int cc = 0; cc < jet.constituents().size(); cc++) { 
    for(int bb = 0; bb < 105; bb++) { 
      if(jet.constituents()[cc].user_index() == bhadronid[bb]) { 
	btagged = true;
	//	cout << "Jet B-tagged!" << endl;
	//	cout << jet << endl;
      }
    }
  }
  return btagged;
}


