
bool HwSimFat::BasicCutsPass(int numparticles, double objects[8][10000], vector<fastjet::PseudoJet> jet, vector<fastjet::PseudoJet> photon, vector<fastjet::PseudoJet> lepton, vector<int> lepton_id, fastjet::PseudoJet pmiss, double cut_eta, double cut_pt_part, double cut_pt_jet, double cut_eta_jet, double cut_pt_muon, double cut_eta_muon, double cut_pt_electron, double cut_eta_electron) {
  
  /* COUNTERS and BOOLEANS
   * for number of events 
   * that pass cuts
   */
  //COUNTERS
  // double N_pass_isolepton(0);

  //BOOLEANS
  /* bool passed_isolepton(true);
  bool passed_bjets(true);
  bool passed_mbb(true);
  bool passed_nlightjets(true);
  bool passed_deltarbb(true);*/


  /* PARAMETERS & SWITCHES 
   * DEFINED HERE
   */
  /*  double pt_iso_lepton_min(40.);

  double pt_bjet1_min(40.);
  double pt_bjet2_min(40.);
  
  double mbb_min(100.);
  double mbb_max(150.);

  double deltarbb12_min(0.);
  double deltarbb12_max(1E5);*/
  


  /* CUTS:
   * DEFINED HERE [GeV]
   */

  /* lepton isolation criteria */
  //double R_lepton_iso(0.2); //lepton isolation cone
  //  double FracFac_lepton_iso(0.15); //lepton isolation fraction

  /* missing transverse energy */
  // double cut_met(-100.);
  
  // double cut_N_leptons_exclusive(1); //number of leptons (exclusive, i.e. exactly equal to this number otherwise reject)

  /* LEPTON ISOLATION: ASK FOR 
   * at least 4 iso LEPTON WITH FracFac_lepton_iso(0.X)xpT AROUND IT, 
   * IN CONE OF R_lepton_iso(0.X);
   */
  /*  vector<fastjet::PseudoJet> electron_iso, muon_iso, amuon_iso, positron_iso, electron_iso_unsorted, muon_iso_unsorted, amuon_iso_unsorted, positron_iso_unsorted; 

  fastjet::PseudoJet pell_hardest(0,0,0,0), pcurr(0,0,0,0);
  double *leptonisovar = new double[lepton.size()];
  int hardest_id(0);
  int n_iso_leptons(0);
  double pt_iso_lepton_max(0.);
  for(int ll = 0; ll < lepton.size(); ll++) { 
    leptonisovar[ll] = 0.;
    for(int pp = 0; pp < numparticles; pp++) {   
      pcurr = fastjet::PseudoJet(objects[1][pp], objects[2][pp], objects[3][pp], objects[0][pp]);
      pcurr.set_user_index(int(objects[4][pp]));
      if(fabs(pcurr.eta()) < cut_eta && pcurr.perp() > cut_pt_part && fabs(objects[4][pp]) != 12 && fabs(objects[4][pp]) != 14 && fabs(objects[4][pp]) != 16 && fabs(objects[4][pp]) != 11 && fabs(objects[4][pp]) != 13 && deltaR(lepton[ll],pcurr) < R_lepton_iso) {
        leptonisovar[ll] += pcurr.perp(); 
      }
    }
    //cout << "lepton isolation: " << ll << " : " << leptonisovar[ll]/lepton_pt[ll].perp() << endl;
    if(leptonisovar[ll]/lepton[ll].perp() < FracFac_lepton_iso) {
      //only counts as isolated lepton if pT is above certain threshold
      if(lepton[ll].perp() > pt_iso_lepton_min) {
        n_iso_leptons++;
        if(lepton_id[ll] == 11) { electron_iso_unsorted.push_back(lepton[ll]); }
        if(lepton_id[ll] == -11) { positron_iso_unsorted.push_back(lepton[ll]); }
        if(lepton_id[ll] == 13) { muon_iso_unsorted.push_back(lepton[ll]); }
        if(lepton_id[ll] == -13) { amuon_iso_unsorted.push_back(lepton[ll]); }
      }
    } 
  }
  delete leptonisovar;

  electron_iso = sorted_by_pt(electron_iso_unsorted);
  positron_iso = sorted_by_pt(positron_iso_unsorted);

  muon_iso = sorted_by_pt(electron_iso_unsorted);
  amuon_iso = sorted_by_pt(electron_iso_unsorted);

  


  int nlightjets = 0;
  //find the bjets
  vector<fastjet::PseudoJet> bjets;
  for(int z = 0; z < jet.size(); z++) {
    if(btag_hadrons(jet[z])) {
      bjets.push_back(jet[z]);
    }
    else { nlightjets++; }
  }
  */
  //make sure there are at least two b-jets
  //reconstruct the invariant mass of the two hardest b-jets
  //this is only OK for the irreducible backgrounds!
  // if(bjets.size()<2) { passed_bjets = false; }
  //if(!passed_bjets) { return 0; }


  //and veto events without at least two extra light jets beyond the b-jets
  /*if(nlightjets<2) { passed_nlightjets = false; }
  
  double mbb12(-1.);
  if(bjets.size()>1) {
    mbb12 = (bjets[0]+bjets[1]).m();
    if(bjets[0].perp() < pt_bjet1_min) { passed_bjets = false; }
    if(bjets[1].perp() < pt_bjet2_min) { passed_bjets = false; }
  }
  if(mbb12 < mbb_min || mbb12 > mbb_max) { passed_mbb = false; }

  double deltarbb12 = deltaR(bjets[0], bjets[1]);
  if(deltarbb12 < deltarbb12_min || deltarbb12 > deltarbb12_max) { passed_deltarbb = false; }
  */
  //veto events that do not contain exactly one isolated lepton
  //if(n_iso_leptons!=1) { /*cout << "number of iso leptons = " << n_iso_leptons << endl;*/ passed_isolepton = false; }
  
  //if(pmiss.perp() > cut_met) { passed_met = true; }   

  //if(!passed_met) { return 0; }
  //N_pass_met++;
/*
  if(!passed_isolepton) { return 0; }
  if(!passed_nlightjets) { return 0; }
  if(!passed_mbb) { return 0; }
  if(!passed_deltarbb) { return 0; }
*/
  //N_pass_isolepton++;
 
  /* FOR EVENTS THAT PASS CUTS
   * PRINT INFORMATION
   * SET THE FLAG AS PASSED
   * DO EXTRA STUFF (e.g. PLOTS)
   */
  return true;
}

pair<vector<fastjet::PseudoJet>,vector<double> > HwSimFat::FatJetAnalysis(int numparticles, double objects[8][10000], vector<fastjet::PseudoJet> jet, vector<fastjet::PseudoJet> photon, vector<fastjet::PseudoJet> lepton, vector<int> lepton_id, fastjet::PseudoJet pmiss, double cut_eta, double cut_pt_part, double cut_pt_jet, double cut_eta_jet, double cut_pt_muon, double cut_eta_muon, double cut_pt_electron, double cut_eta_electron) {

  /*jet algorithm */
  double R(FatR_);
  
  /* particles to exclude from jet finder:
   * neutrinos
   */
  int excluded[6] = { 12, -12, 14, -14, 16, -16 } ;
  
  vector<int> exclude_ids;
  for(int ex = 0; ex < 6; ex++) exclude_ids.push_back(excluded[ex]);
						
  fastjet::PseudoJet pcurr; //the current particle under consideration
  vector<fastjet::PseudoJet> input_particles; //the input particles to the jet finder

  /* loop over particles in event
   * decide which are "photons" and "leptons", 
   * according to isolation criteria. 
   * these are then excluded from the jet finder. 
   */
  bool exclude_particle(false);
  for(int pp = 0; pp < numparticles; pp++) {
    
    //put the current particle into a pseudojet vector
    pcurr = fastjet::PseudoJet(objects[1][pp], objects[2][pp], objects[3][pp], objects[0][pp]);
    pcurr.set_user_index(int(objects[4][pp]));
   
    // if the particle is included in the array above, flag as excluded from particles to run over jet-finding
    for(int idl = 0; idl < exclude_ids.size(); idl++) {
      if(fabs(objects[4][pp]) == exclude_ids[idl]) { exclude_particle = true; }
    }
            
    /* FIND HARD LEPTONS 
     * if they do not satisfy the cuts, put them into the jet finder. 
     */
    fastjet::PseudoJet pcurr_charged; //the current particle under consideration
    //loop over particles
    if(fabs(objects[4][pp]) == 11 && pcurr.perp() > cut_pt_electron && fabs(pcurr.eta()) < cut_eta_electron) {
      if(!electronisolation_) {
        lepton.push_back(pcurr); 
	lepton_id.push_back(int(objects[4][pp]));
        exclude_particle = true;
      } else if(electronisolation_) {
	bool iso_electron = 1;
	double sum_pt_tracks = 0;
	for(int yy = 0; yy < numparticles; yy++) {
	  if(objects[5][yy]!=0 && yy!=pp) {//if charged particle
	    pcurr_charged = fastjet::PseudoJet(objects[1][yy], objects[2][yy], objects[3][yy], objects[0][yy]);
	    if(deltaR(pcurr, pcurr_charged) < electronIsoR_)
	      sum_pt_tracks += pcurr_charged.perp();
	  }
	}
	if(sum_pt_tracks/pcurr.perp() < electronIsoFrac_) {
	  //cout << "electron isolation = " << sum_pt_tracks/pcurr.perp()  << endl;	
	  iso_electron = 1;
	  lepton.push_back(pcurr); 
	  lepton_id.push_back(int(objects[4][pp]));
	  exclude_particle = true;
	}	  
      }
    }
	
    if(fabs(objects[4][pp]) == 13 && pcurr.perp() > cut_pt_muon && fabs(pcurr.eta()) < cut_eta_muon) {
      if(!muonisolation_) {
        lepton.push_back(pcurr); 
	lepton_id.push_back(int(objects[4][pp]));
        exclude_particle = true;
      } else if(muonisolation_) {
	bool iso_muon = 1;
	double sum_pt_tracks = 0;
	for(int yy = 0; yy < numparticles; yy++) {
	  if(objects[5][yy]!=0&&yy!=pp) {//if charged particle
	    pcurr_charged = fastjet::PseudoJet(objects[1][yy], objects[2][yy], objects[3][yy], objects[0][yy]);
	    if(deltaR(pcurr, pcurr_charged) < muonIsoR_)
	      sum_pt_tracks += pcurr_charged.perp();
	  }
	}
	if(sum_pt_tracks/pcurr.perp() < muonIsoFrac_) {
	  //cout << "muon isolation = " << sum_pt_tracks/pcurr.perp()  << endl;	
	  iso_muon = 1;
	  lepton.push_back(pcurr); 
	  lepton_id.push_back(int(objects[4][pp]));
	  exclude_particle = true;
	}	  
      }
    }
    
    //push the visible particles into the jet finding input and calculate the missing energy 4-vector
    if(fabs(pcurr.eta()) < cut_eta && pcurr.perp() > cut_pt_part && !exclude_particle) {
      input_particles.push_back(pcurr);
    }
    
    //reset the exclude_particle flag
    exclude_particle = false;	    
  } /* LOOP OVER PARTICLES OF EVENT
     * ENDS
     * HERE 
     */

  /* START JET-FINDING
   * USING FASTJET
   */
  fastjet::RecombinationScheme recombinationScheme = fastjet::E_scheme;
  fastjet::Strategy            strategy            = fastjet::Best;
  fastjet::JetDefinition theJetDefinition;

 
  //first figure out which algorithm is chosen
  switch (jetAlgorithm_) {
  case  -1: theJetDefinition=fastjet::JetDefinition(fastjet::antikt_algorithm,
						    R,
						    recombinationScheme,
						    strategy); break;
  case   0: theJetDefinition=fastjet::JetDefinition(fastjet::cambridge_algorithm,
						    R,
						    recombinationScheme,
						    strategy); break;
  case   1: theJetDefinition=fastjet::JetDefinition(fastjet::kt_algorithm,
						    R,
						    recombinationScheme,
						    strategy); break;
  }
  fastjet::ClusterSequence clust_seq(input_particles, theJetDefinition);

  vector<fastjet::PseudoJet> fatjets = sorted_by_pt(clust_seq.inclusive_jets(cut_pt_jet));

  vector<fastjet::PseudoJet> fatjets_pruned;
  vector<double> tau21;
  
  /* Prune the fat jets if required */
  if(pruneFat_) {
    vector<fastjet::Transformer *> groomers;
    groomers.push_back(new Pruner(fastjet::cambridge_algorithm, zcut_, rcut_factor_));
    double beta = 1.0;
    fastjet::contrib::NsubjettinessRatio   nSub21_beta1(2,1, fastjet::contrib::OnePass_WTA_KT_Axes(), fastjet::contrib::UnnormalizedMeasure(beta));
    const fastjet::Transformer & f = *groomers[0];
    for (vector<PseudoJet>::iterator jit=fatjets.begin(); jit!=fatjets.end(); jit++){
      const fastjet::PseudoJet & c = *jit;
      fastjet::PseudoJet j = f(c);
      double tau21_beta1 = nSub21_beta1(j);
      tau21.push_back(tau21_beta1);
      //cout << "pt, mass, tau21= " << j.perp() << " " << j.m() << " " << tau21_beta1 << endl;
      fatjets_pruned.push_back(j);
    }
    fatjets_pruned = sorted_by_pt(fatjets_pruned);
    delete groomers[0];
  }

  pair<vector<fastjet::PseudoJet>,vector<double> > fatjets_tau;

  if(!pruneFat_) {
    pair<vector<fastjet::PseudoJet>,vector<double> > fatjets_tau;
    fatjets_tau.first = fatjets;
    fatjets_tau.second = tau21;
  }
  else {

      fatjets_tau.first = fatjets_pruned;
      fatjets_tau.second = tau21;
  }
  return fatjets_tau;
}
  
